# キュレーションメディア

キュレーションメディアのテンプレートを作成しました。
主な機能は下記の通りです。
- 記事の作成、編集、削除
  - 画像、動画の埋め込みも可能
  - WYSIWYG
- 管理画面（３つの権限）
- SNSシェア（FB, TW, LINE, hatena, mixi）
- PVカウント
- アクセスランキング
- タグ機能（タグページ）

## CSSフレームワーク
- http://materializecss.com/

## 環境
- ruby 2.1.4p265
- Rails 4.1.5
- Capistrano 3.3.3
