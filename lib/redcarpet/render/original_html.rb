class Redcarpet::Render::OriginalHTML < Redcarpet::Render::HTML
  def header(text, level)
    level += 1
    "<h#{level}>#{text}</h#{level}>"
  end

  def link(link, title, alt_text)
    "<a target=\"_blank\" ref=\"nofollow\" href=\"#{link}\">#{alt_text}</a>"
  end

  def autolink(link, link_type)
    "<a target=\"_blank\" ref=\"nofollow\" href=\"#{link}\">#{link}</a>"
  end
end
