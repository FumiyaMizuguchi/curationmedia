# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141217095814) do

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role",                   default: 0
    t.string   "name"
    t.string   "nickname"
    t.text     "introduction"
    t.string   "image_url"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "article_tags", force: true do |t|
    t.integer  "article_id", null: false
    t.integer  "tag_id",     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "article_tags", ["article_id"], name: "index_article_tags_on_article_id", using: :btree
  add_index "article_tags", ["tag_id"], name: "index_article_tags_on_tag_id", using: :btree

  create_table "articles", force: true do |t|
    t.integer  "admin_user_id"
    t.string   "title"
    t.text     "description"
    t.text     "content"
    t.string   "main_image_url"
    t.boolean  "is_draft",       default: false
    t.boolean  "is_public",      default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "articles", ["is_draft", "is_public", "created_at"], name: "open_articles_index", using: :btree
  add_index "articles", ["is_draft"], name: "index_articles_on_is_draft", using: :btree

  create_table "footprints", force: true do |t|
    t.integer  "article_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "footprints", ["article_id"], name: "index_footprints_on_article_id", using: :btree

  create_table "tags", force: true do |t|
    t.string   "name"
    t.string   "ancestry"
    t.text     "body"
    t.integer  "articles_count", default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["ancestry"], name: "index_tags_on_ancestry", using: :btree

end
