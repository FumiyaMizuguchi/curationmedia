class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :admin_user_id
      t.string :title
      t.text :description
      t.text :content
      t.string :main_image_url
      t.boolean :is_draft, default: false
      t.boolean :is_public, default: true

      t.timestamps
    end
    add_index :articles, :is_draft
  end
end
