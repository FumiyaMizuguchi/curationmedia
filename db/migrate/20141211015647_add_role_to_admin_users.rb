class AddRoleToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :role, :integer, default: 0
    add_column :admin_users, :name, :string
    add_column :admin_users, :nickname, :string
    add_column :admin_users, :introduction, :text
    add_column :admin_users, :image_url, :string
  end
end
