class CreateFootprints < ActiveRecord::Migration
  def change
    create_table :footprints do |t|
      t.integer :article_id, null: false

      t.timestamps
    end

    add_index :footprints, :article_id
  end
end
