class AddIndexToArticles < ActiveRecord::Migration
  def change
    add_index :articles, [:is_draft, :is_public, :created_at], :name => 'open_articles_index'
  end
end
