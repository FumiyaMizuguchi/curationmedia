class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string  :name
      t.string  :ancestry
      t.text    :body
      t.integer :articles_count, default: 0, null: false

      t.timestamps
    end

    add_index :tags, :ancestry
  end
end
