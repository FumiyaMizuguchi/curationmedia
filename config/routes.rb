Rails.application.routes.draw do
  devise_for :admin_users, :controllers => {
    :registrations => 'admin_users/registrations',
    :sessions => 'admin_users/sessions'
  }

  root to: 'articles#index'
  resources :articles, only: [:index, :show]
  resources :tags, param: :name, only: :show, :constraints => { :name => /[^\/]*/ }
  get 'admin' => 'admin/base#index'
  get 'admin/manager/articles' => 'admin/manager#articles'
  get 'admin/manager/rankings' => 'admin/manager#rankings'
  get 'admin/manager/admin_users' => 'admin/manager#admin_users'
  post 'admin/manager/change_authority' => 'admin/manager#change_authority'

  namespace :admin do
    resources :articles do
      collection do
        post 'preview'
      end
    end
    resources :tags
  end
end
