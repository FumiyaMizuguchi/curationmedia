require 'exception_notification/rails'

ExceptionNotification.configure do |config|
  config.ignore_if do |exception, options|
    not Rails.env.production?
  end

  config.add_notifier :slack, {
    :webhook_url => 'https://hooks.slack.com/services/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',
    :channel => '#omps',
    :link_names => true
  }
end
