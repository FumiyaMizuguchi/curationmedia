// Button to go to top
var topBtn = $('#js-goto-top');
topBtn.hide();
// show button when scrolled down
$(window).scroll(function () {
  var scrollHeight = $(document).height();
  var scrollPosition = $(window).height() + $(window).scrollTop();
  if ((scrollHeight - scrollPosition) < 1000) {
    // when scroll to bottom of the page
    topBtn.fadeIn();
  } else {
    topBtn.fadeOut();
  }
});
// go to top
topBtn.click(function () {
  $('body,html').animate({
    scrollTop: 0
  }, 500);
  return false;
});
