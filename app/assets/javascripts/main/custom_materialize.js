$(function(){
  $(".button-collapse").sideNav();
  $("form :input").focus(function() {
    $("label[for='" + this.id + "']").addClass("active");
  });
  $("form :input").focusout(function() {
    if($(this).val().length == 0) {
    $("label[for='" + this.id + "']").removeClass("active");
    }
  });
});
