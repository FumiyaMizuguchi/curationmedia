$(function() {
  $('#article_content').editable({
  inlineMode: false,
  toolbarFixed: false,
  shortcuts: true,
  shortcutsAvailable: ['bold', 'italic', 'underline'],
  minHeight: 300,
  buttons : ["insertH2", "insertH3", "refer", "quote", "insertImage", "insertVideo",  "bold", "italic", "underline", "strikeThrough", "fontSize", "color", "formatBlock", "blockStyle", "align", "createLink", "undo", "redo", "html", "save", "insertHorizontalRule", "table", "clear"],

  customButtons: {
      clear: {
        title: 'Clear HTML',
        icon: {
          type: 'txt',
          value: 'クリア'
        },
        callback: function () {
          this.setHTML('');
          this.focus();
        },
        refresh: function () { }
      },

      insertH2: {
        title: 'Insert HTML',
        icon: {
          type: 'txt',
          value: '見出し'
        },
        callback: function () {
          // Insert HTML.
          this.insertHTML("<h2>見出し</h2>");

          // Save HTML in undo stack.
          this.saveUndoStep();
        },
        refresh: function () { }
      },

      insertH3: {
        title: 'Insert HTML',
        icon: {
          type: 'txt',
          value: '小見出し'
        },
        callback: function () {
          // Insert HTML.
          this.insertHTML("<h3>小見出し</h3>");

          // Save HTML in undo stack.
          this.saveUndoStep();
        },
        refresh: function () { }
      },

      refer: {
        title: 'Insert HTML',
        icon: {
          type: 'txt',
          value: '出典'
        },
        callback: function () {
          // Insert HTML.
          this.insertHTML("<p class='refer'>出典: http://xxx.com</p><p></p>");

          // Save HTML in undo stack.
          this.saveUndoStep();
        },
        refresh: function () { }
      },

      quote: {
        title: 'Insert HTML',
        icon: {
          type: 'txt',
          value: '引用文'
        },
        callback: function () {
          // Insert HTML.
          this.insertHTML("<blockquote>引用文（引用文の改行はShift+Enter）</blockquote>");

          // Save HTML in undo stack.
          this.saveUndoStep();
        },
        refresh: function () { }
      }
    }
  })
});
