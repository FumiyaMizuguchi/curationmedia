jQuery(function($) {
  
var nav    = $('#fixedBox'),
    offset = nav.offset();
  
$(window).scroll(function () {
  var scrollHeight = $(document).height();
  var scrollPosition = $(window).height() + $(window).scrollTop();
  if($(window).scrollTop() > offset.top) {
    nav.addClass('fixed');
  } else if ((scrollHeight - scrollPosition) < 100) {
    nav.removeClass('fixed');
  } else {
    nav.removeClass('fixed');
  }
});
  
});
