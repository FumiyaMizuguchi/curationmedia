$('#article_description').bind('keydown keyup keypress change',function(){
  var thisValueLength = $(this).val().length;
  if (thisValueLength < 100) {
    $('.js-desc-counter').css({color:'#000',fontWeight:'normal',fontSize: '12px'});
  }
  if (thisValueLength > 100) {
    $('.js-desc-counter').css({color:'#ff0000',fontWeight:'bold'});
  }
  if (thisValueLength > 130) {
    $('.js-desc-counter').css({color:'#ff0000',fontWeight:'bold',fontSize: '16px'});
  }
  $('.js-desc-counter').html(thisValueLength);
});
$('#article_title').bind('keydown keyup keypress change',function(){
  var thisValueLength = $(this).val().length;
  if (thisValueLength < 40) {
    $('.js-title-counter').css({color:'#000',fontWeight:'normal',fontSize: '12px'});
  }
  if (thisValueLength > 40) {
    $('.js-title-counter').css({color:'#ff0000',fontWeight:'bold'});
  }
  $('.js-title-counter').html(thisValueLength);
});
