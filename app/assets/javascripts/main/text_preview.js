$('#js-reload-preview').on('click', function () {
  text = $('#article_content').val();
  var title = $('#article_title').val();
  var description = $('#article_description').val();
  $('#title-preview').html(title);
  $('#desc-preview').html(description);
  $('.preloader-wrapper').addClass('active');
  $.ajax({
    method: 'POST',
    url: '/admin/articles/preview',
    data: {
      text: text
    },
    success: function(data) {
      console.log('success')
      $('#content-preview').html(data)
      $('.preloader-wrapper').delay(800).queue(function() {
        $(this).removeClass('active');
        $(this).dequeue();
      });
    },
    error: function(data) {
      console.log('error')
    }
  });
});
