$ ->
  Chart.defaults.global.responsive = true
  date_list = []
  count_list = []
  _.each( gon.articles, (value) =>
    date_list.push value[0]
    count_list.push value[1]
  )
  data = {
    labels: date_list
    datasets : [
      {
        fillColor : "rgba(210,82,127,1)",
        strokeColor : "rgba(210,82,127,1)",
        pointColor : "rgba(255,179,0,1)",
        pointStrokeColor : "#fff",
        data: count_list
      }
    ]
  }

  myNewChart = new Chart($("#canvas").get(0).getContext("2d")).Bar(data)

  date_list = []
  count_list = []
  gon.each_user_articles_count.reverse()
  _.each( gon.each_user_articles_count, (value) =>
    date_list.push value[0]
    count_list.push value[1]
  )
  second_data = {
    labels: date_list
    datasets : [
      {
        fillColor : "rgba(255,235,59,1)",
        strokeColor : "rgba(255,235,59,1)",
        pointColor : "rgba(255,179,0,1)",
        pointStrokeColor : "#fff",
        data: count_list
      }
    ]
  }

  myNewChart = new Chart($("#articles_count_ranking").get(0).getContext("2d")).Bar(second_data)

  date_list = []
  count_list = []
  gon.each_user_footprints_count.reverse()
  _.each( gon.each_user_footprints_count, (value) =>
    date_list.push value[0]
    count_list.push value[1]
  )
  third_data = {
    labels: date_list
    datasets : [
      {
        fillColor : "rgba(38,198,218,1)",
        strokeColor : "rgba(38,198,218,1)",
        pointColor : "rgba(255,179,0,1)",
        pointStrokeColor : "#fff",
        data: count_list
      }
    ]
  }

  myNewChart = new Chart($("#footprints_count_ranking").get(0).getContext("2d")).Bar(third_data)
