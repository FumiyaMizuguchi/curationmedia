$ ->
  $('.js-change-role').on('change', ->
    console.log 'changed'
    user_id = $(this).attr('data-id')
    value = $(":selected").attr("value")
    $.ajax(
      method: 'POST',
      url: '/admin/manager/change_authority',
      data: {
        value: value,
        admin_user_id: user_id
      },
      success: (data) ->
        console.log 'success'
        $('#admin_users_list').html(data.html)
      error: (data) ->
        console.log 'error'
    )
  )
