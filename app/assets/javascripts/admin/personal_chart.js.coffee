$ ->
  Chart.defaults.global.responsive = true
  date_list = []
  count_list = []
  _.each( gon.personal_articles, (value) =>
    date_list.push value[0]
    count_list.push value[1]
  )
  p_data = {
    labels: date_list
    datasets : [
      {
        fillColor : "rgba(210,82,127,1)",
        strokeColor : "rgba(210,82,127,1)",
        pointColor : "rgba(255,179,0,1)",
        pointStrokeColor : "#fff",
        data: count_list
      }
    ]
  }
  parsonalChart = new Chart($("#canvas2").get(0).getContext("2d")).Bar(p_data)
