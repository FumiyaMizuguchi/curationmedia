class Article < ActiveRecord::Base
  belongs_to :admin_user
  has_many :article_tags
  has_many :tags, through: :article_tags
  has_many :footprints

  validates :title, presence: true
  validates :description, presence: true
  validates :content, presence: true
  validates :main_image_url, presence: true, length: { maximum: 200 }

  class << self
    def sort_by_footprints
      joins(:footprints).group(:article_id).order('SUM(articles.id) DESC')
    end

    def published
      where(is_draft: false, is_public: true)
    end

    def draft
      where(is_draft: true)
    end

    def closed
      where(is_public: false)
    end

    def yesterday
      where("articles.created_at >= ?", Date.yesterday.beginning_of_day).where('articles.created_at < ?', Date.today.beginning_of_day)
    end

    def each_day_counts_list(user_id=nil)
      start_date = Date.new(2015, 01, 07)
      end_date = Date.new(2015, 01, 31)
      day_range = start_date..end_date
      [].tap do |ary|
        day_range.each do |day|
          ary << [day, where(admin_user_id: user_id).where('created_at >= ?', day.beginning_of_day).where('created_at < ?', day.tomorrow.beginning_of_day).count.to_i]
        end
      end
    end

    def all_each_day_counts_list
      start_date = Date.new(2015, 01, 07)
      end_date = Date.new(2015, 01, 31)
      day_range = start_date..end_date
      [].tap do |ary|
        day_range.each do |day|
          ary << [day, where('created_at >= ?', day.beginning_of_day).where('created_at < ?', day.tomorrow.beginning_of_day).count.to_i]
        end
      end
    end

    def each_user_articles_count_list
      start_date = Date.new(2015, 01, 07)
      end_date = Date.new(2015, 01, 31)
      ary = []
      AdminUser.all.each do |user|
        if user.manager? || user.member?
          ary << [user.name, published.where(admin_user_id: user.id).where('created_at >=?', start_date).where('created_at < ?', end_date).count]
        end
      end
      ary.sort { |a,b|
        a[1] <=> b[1]
      }
    end
  end
end
