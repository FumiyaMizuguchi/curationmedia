class Footprint < ActiveRecord::Base
  belongs_to :article

  class << self
    def visit!(article)
      create!(article: article)
    end
  end
end
