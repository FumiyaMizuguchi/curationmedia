class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  enum role: {writer: 0, manager: 1, member: 2}
  has_many :articles

  def footprints_count
    count = 0
    articles.each do |article|
      count += article.footprints.count
    end
    count
  end

  def get_experience_rank
    xp = footprints_count/3000
    if xp == 0
      footprints_count/300
    else
      xp + 10
    end
  end

  def need_exp_to_levelup
    xp = footprints_count/3000
    if xp == 0
      level = footprints_count/300
      300*(level+1)-footprints_count
    else
      level = xp + 10
      3000*(level+1)-footprints_count
    end
  end

  class << self
    def each_user_footprints_count_list
      start_date = Date.new(2015, 01, 07)
      end_date = Date.new(2015, 01, 31)
      t = joins('LEFT JOIN articles ON articles.admin_user_id = admin_users.id')
      t = t.joins('LEFT JOIN footprints ON footprints.article_id = articles.id')
      t = t.group('admin_users.id')
      t = t.where('footprints.created_at >= ?', start_date).where('footprints.created_at < ?', end_date)
      t = t.select('admin_users.*', 'count(footprints.id) as score')
      t = t.order('score desc')

      ary = []
      t.each do |user|
        ary << [user.name, user.score]
      end
      ary.sort { |a,b|
        a[1] <=> b[1]
      }
    end
  end
end
