class Ability
  include CanCan::Ability

  def initialize(user)
    if user.manager?
      can :manager, :all
    elsif user.writer?
      can :writer, :all
    end
  end
end
