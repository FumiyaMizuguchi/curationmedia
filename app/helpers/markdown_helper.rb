module MarkdownHelper
  def markdown(text)
    unless @markdown
      @markdown = Redcarpet::Markdown.new(Redcarpet::Render::OriginalHTML, autolink: true, tables: true)
    end

    @markdown.render(text).gsub(/\n\n/, '<br />').gsub(/\n/, '<br />').html_safe
  end
end
