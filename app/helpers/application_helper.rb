module ApplicationHelper
  def hbr(str)
    h(str).gsub(/(\r\n?)|(\n)/, "<br />").html_safe
  end

  def time_display(datetime)
    datetime.localtime.strftime('%Y年%m月%d日')
  end

  def comma_display(number)
    return number.to_s.reverse.gsub( /(\d{3})(?=\d)/, '\1,' ).reverse
  end
end
