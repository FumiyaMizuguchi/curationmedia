class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def render_500(e)
    ExceptionNotifier.notify_exception(e, :env => request.env, :data => {:message => "error"})
    render template: 'errors/error_500', status: 500
  end

  def render_404(e)
    ExceptionNotifier.notify_exception(e, :env => request.env, :data => {:message => "error"})
    render template: 'errors/error_404', status: 404
  end

protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << [:role, :name, :nickname, :introduction, :image_url]
    devise_parameter_sanitizer.for(:account_update) << [:role, :name, :nickname, :introduction, :image_url]
  end

  def current_ability
    @current_ability ||= Ability.new(current_admin_user)
  end

  def after_sign_in_path_for(resource)
    admin_path
  end
end
