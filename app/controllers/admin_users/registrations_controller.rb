class AdminUsers::RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
    if params[:admin_user][:email]  == Settings.admin.manager
      params[:admin_user][:role] = 1
    end
    super
  end

  def update
    unless @admin_user.manager?
      super
    else
      if admin_user_params[:password].blank?
        admin_user_params.delete(:password)
        admin_user_params.delete(:password_confirmation)
      end

      successfully_updated = if needs_password?(@admin_user, admin_user_params)
                               @admin_user.update(admin_user_params)
                             else
                               @admin_user.update_without_password(admin_user_params)
                             end

      respond_to do |format|
        if successfully_updated
          format.html { redirect_to admin_path, notice: 'User was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @admin_user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

private

  def admin_user_params
    params.require(:admin_user).permit(:name, :nickname, :introduction, :image_url, :email, :password, :password_confirmation)
  end

  def needs_password?(user, params)
    params[:password].present?
  end

end
