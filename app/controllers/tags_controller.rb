class TagsController < ApplicationController
  before_action :set_tag, only: :show

  def show
    @articles = @tag.articles.published.page(params[:page]).per(10)
    @latests = Article.published.order('articles.created_at DESC').limit(5)
    @rankings = Article.published.yesterday.sort_by_footprints.limit(5)
  end

private
  def set_tag
    @tag = Tag.find_by(name: params[:name]) or fail ActiveRecord::RecordNotFound
  end
end
