class Admin::BaseController < ApplicationController
  before_filter :authenticate_admin_user!
  def index
    @published_articles = current_admin_user.articles.published.order('created_at DESC').page(params[:page]).per(10)
    @draft_articles = current_admin_user.articles.draft.order('created_at DESC').page(params[:page]).per(10)

    gon.personal_articles = Article.published.each_day_counts_list(current_admin_user.id)
  end

private
  def manager
    redirect_to admin_path, notice: 'その操作は行えません。' unless current_admin_user.manager?
  end
end
