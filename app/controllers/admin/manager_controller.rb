class Admin::ManagerController < Admin::BaseController
  before_filter :manager, only: [:change_authority]
  def articles
    @search = Article.published.order('created_at DESC').search(params[:q])
    @published_articles = @search.result.page(params[:page]).per(25)
    @draft_articles = Article.draft.order('created_at DESC').page(params[:page]).per(25)
    @closed_articles = Article.closed.order('created_at DESC').page(params[:page]).per(25)
  end

  def rankings
    gon.articles = Article.published.all_each_day_counts_list
    gon.each_user_articles_count = Article.published.each_user_articles_count_list
    gon.each_user_footprints_count = AdminUser.each_user_footprints_count_list
  end

  def admin_users
    @admin_users = AdminUser.all.order('created_at DESC')
  end

  def change_authority
    @admin_user = AdminUser.find_by_id(params[:admin_user_id])

    @admin_users = AdminUser.all.order('created_at DESC')
    if @admin_user != nil
      @admin_user.update(role: params[:value].to_i)

      html = render_to_string partial: 'admin/manager/admin_users_list', collection: [@admin_users]
      flash.now[:notice] = '更新されました。'
      render json: { html: html }
    else
      flash.now[:notice] = 'エラーが発生ました。'
      render json: { error: false }, status: 400
    end

  end
end
