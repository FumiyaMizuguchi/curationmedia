class Admin::ArticlesController < Admin::BaseController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_filter :manager, only: :destroy
  before_filter :disable_xss_protection
  include MarkdownHelper

  def index
    @published_articles = current_admin_user.articles.published.order('created_at DESC').page(params[:page]).per(10)
    @draft_articles = current_admin_user.articles.draft.order('created_at DESC').page(params[:page]).per(10)
    @closed_articles = current_admin_user.articles.closed.order('created_at DESC').page(params[:page]).per(10)
  end

  def show
  end

  def new
    @article = Article.new()
  end

  def edit
  end

  def create
    @article = Article.new(article_params)
    @article.admin_user = current_admin_user

    if @article.save
      redirect_to admin_path, notice: 'お疲れ様です！記事が作成されました :)'
    else
      render :new
    end
  end

  def update
    if @article.update(article_params)
      redirect_to admin_article_path(@article), notice: 'お疲れ様です！無事に更新されました！ :D'
    else
      render :edit
    end
  end

  def destroy
    @article.destroy
    redirect_to admin_manager_articles_path, notice: "「#{@article.title}」の記事を削除しました。"
  end

  def preview
    render text: markdown(params[:text])
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  def article_params
    @post_params ||= begin
      param_hash = params.require(:article).permit(:title, :description, :content, :tags, :main_image_url, :is_draft, :is_public).to_hash

      # tags_text == 'Javascript,Ruby'
      tags_text = param_hash.delete('tags')

      tags = tags_text.split(',').map do |tag_name|
        Tag.find_or_create_by(name: tag_name)
      end
      param_hash['tag_ids'] = tags.map(&:id)

      param_hash
    end
  end
protected
  def disable_xss_protection
    # Disabling this is probably not a good idea,
    # but the header causes Chrome to choke when being
    # redirected back after a submit and the page contains an iframe.
    response.headers['X-XSS-Protection'] = "0"
  end
end
