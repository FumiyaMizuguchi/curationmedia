class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

  # GET /articles
  def index
    @articles = Article.published.order('updated_at DESC').page(params[:page]).per(10)
    @latests = Article.published.order('articles.created_at DESC').limit(5)
    @rankings = Article.published.yesterday.sort_by_footprints.limit(5)
  end

  # GET /articles/1
  def show
    redirect_to articles_path if @article.is_draft?
    Footprint.visit!(@article)
    @rankings = Article.published.yesterday.sort_by_footprints.limit(5)
    @latests = Article.published.order('created_at DESC').limit(5)
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article, notice: 'お疲れ様です！記事が作成されました :)'
    else
      render :new
    end
  end

  # PATCH/PUT /articles/1
  def update
    if @article.update(article_params)
      redirect_to @article, notice: 'お疲れ様です！無事に更新されました！ :D'
    else
      render :edit
    end
  end

  # DELETE /articles/1
  def destroy
    @article.destroy
    redirect_to articles_url, notice: '記事を削除しました ;('
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def article_params
      params.require(:article).permit(:title, :description, :content, :main_image_url, :is_draft)
    end
end
