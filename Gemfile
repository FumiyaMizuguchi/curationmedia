source 'https://rubygems.org'

gem 'rails', '4.1.5'
gem 'mysql2'
gem 'uglifier', '>= 1.3.0'
gem 'activerecord-import'
gem 'therubyracer', platforms: :ruby
gem 'rails_config'
# gem 'turbolinks'

gem 'devise'
gem 'cancancan'
gem 'enum_help'
gem 'ransack'

### For front-end ###
gem 'slim-rails'
gem 'sass-rails', '~> 4.0.3'
gem 'coffee-rails', '~> 4.0.0'
gem 'compass-rails'
gem 'compass'
gem 'less-rails'
gem 'execjs'
gem 'font-awesome-rails'
gem 'jquery-rails'

### Markdown ###
gem "redcarpet", "~> 2.3.0"

### tree structure ###
gem 'ancestry'

### pagenation ###
gem 'kaminari'

### richtext ###
gem "wysiwyg-rails"

### alert ###
gem 'slack-notifier'
gem 'exception_notification', git: 'https://github.com/smartinez87/exception_notification.git'

### graph ###
gem 'chart-js-rails'
gem "gon"
gem 'underscore-rails'

group :development do
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv'
end

group :test do
  gem 'rack_session_access'
end

group :test, :development do
  gem 'byebug'
  gem 'awesome_print'
  gem 'pry-rails'
  gem 'rubocop'

  # Error Screen Optimizer
  gem 'better_errors'
  gem 'binding_of_caller'

  # Active Record Formatter
  gem 'hirb'
  gem 'hirb-unicode'

  # Log
  gem 'quiet_assets'
  gem 'colorize_unpermitted_parameters'
  gem 'rails-clean-logs'

  # Test Tools
  gem 'rspec-rails'
  gem 'capybara'
  gem 'poltergeist'
  gem 'faker', '~> 1.1.2'
  gem 'email_spec'
  gem 'factory_girl_rails'
  gem 'database_rewinder'
end
